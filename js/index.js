let users = [
    {
        id: "123456789",
        createdDate: "2021-01-06T00:00:00.000Z",
        status: "En validation",
        firstName: "Mohamed",
        lastName: "Taha",
        userName: "mtaha",
        registrationNumber: "2584",
    },
    {
        id: "987654321",
        createdDate: "2021-07-25T00:00:00.000Z",
        status: "Validé",
        firstName: "Hamid",
        lastName: "Orrich",
        userName: "horrich",
        registrationNumber: "1594",
    },
    {
        id: "852963741",
        createdDate: "2021-09-15T00:00:00.000Z",
        status: "Rejeté",
        firstName: "Rachid",
        lastName: "Mahidi",
        userName: "rmahidi",
        registrationNumber: "3576",
    }
]

// Selecting the container that will contain the table
const appTable = document.getElementById("app-table");

// Table rendering function
function renderTable() {
    // Setting up rows
    let rows = "";
    try {
        for (let i = 0; i < users.length; i++) {
            let tableRowBottomBorder = "app-user-table-row";
            if (i === users.length - 1)
                tableRowBottomBorder = "";
            let row = `<tr class=${tableRowBottomBorder}>
        <td class="app-user-table-data app-text-left">${users[i].id}</td>
        <td class="app-user-table-data app-text-left">${formatDate(users[i].createdDate)}</td>
        <td class="app-user-table-data app-text-center">${formatStatus(users[i].status)}</td>
        <td class="app-user-table-data app-text-left">${users[i].firstName}</td>
        <td class="app-user-table-data app-text-left">${users[i].lastName}</td>
        <td class="app-user-table-data app-text-left">${users[i].userName}</td>
        <td class="app-user-table-data app-text-left">${users[i].registrationNumber}</td>
        <td class="app-user-table-data app-text-center">
            <div onclick="deleteUser(${users[i].id})"><img class="app-user-table-delete-icon" src='./icons/delete.svg'></div>
        </td>
        </tr>`;
            rows += row;
        }
        // Setting up the display table 
        let table =
            `<table class="app-w-full app-user-table">
            <thead>
                <tr class="app-user-table-row">
                    <th class="app-user-table-data app-w-10 app-text-left">ID</th>
                    <th class="app-user-table-data app-w-10 app-text-left">Date de création</th>
                    <th class="app-user-table-data app-w-10 app-text-center">Etat</th>
                    <th class="app-user-table-data app-w-10 app-text-left">Nom</th>
                    <th class="app-user-table-data app-w-10 app-text-left">Prénom</th>
                    <th class="app-user-table-data app-w-10 app-text-left">Nom d'utilisateur</th>
                    <th class="app-user-table-data app-w-10 app-text-left">Matricule</th>
                    <th class="app-user-table-data app-w-10 app-text-center">Action</th>
                </tr>
            </thead>
            <tbody>
            ${rows}
            </tbody>
            </table>`;
        // Displaying table
        appTable.innerHTML = table;
        console.log(users);
    } catch (e) {
        console.log(e);
    }
}

// First render
renderTable();

// Function to format the date to the format dd/mm/yyyy
function formatDate(date) {
    let shortDate = new Date(date);
    shortDate = shortDate.toLocaleDateString('en-US', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
    });
    return shortDate;
}

function formatStatus(status) {
    switch (status) {
        case "En validation":
            return `<div class='on-validation app-user-status app-inline-block'>${status}</div>`;
        case "Validé":
            return `<div class='valide app-user-status app-inline-block'>${status}</div>`;
        case "Rejeté":
            return `<div class='rejected app-user-status app-inline-block'>${status}</div>`;
        default:
            return `<div class=''>${status}</div>`;
    }
}

function deleteUser(id) {
    try {
        for (let i = 0; i < users.length; i++) {
            if (users[i].id.localeCompare(id) === 0) {
                users.splice(i, 1);
                break;
            }
        }
        renderTable();
    }
    catch (e) {
        console.log(e);
    }
}

// Selecting the modal that will contain the user adding inputs
const appModal = document.getElementById("app-modal-backdrop");

function showModal() {
    appModal.style.display = "block";
}

function hideModal() {
    appModal.style.display = "none";
}

// Selecting the modal inputs
const lastName = document.getElementById("lastName");
const firstName = document.getElementById("firstName");
const createdDate = document.getElementById("createdDate");
const userStatus = document.getElementById("status");
const userName = document.getElementById("userName");
const registrationNumber = document.getElementById("registrationNumber");

function saveUser() {
    if (lastName.value && firstName.value && createdDate.value && userStatus.value && userName.value && registrationNumber.value) {
        if (isNaN(Date.parse(createdDate.value))) {
            alert("Verifiez le format de date de création ! :)");
        }
        else {
            hideModal();
            users.push(
                {
                    id: Date.now().toString(),
                    createdDate: Date.parse(createdDate.value),
                    status: userStatus.value,
                    firstName: firstName.value,
                    lastName: lastName.value,
                    userName: userName.value,
                    registrationNumber: registrationNumber.value,
                }
            );
            renderTable();
        }
    }
    else {
        alert("Verifiez les inputs ! :)");
    }
}
